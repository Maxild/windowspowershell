$root         = Split-Path $profile
$modules      = Join-Path $root "Modules"
$scripts      = Join-Path $root "Scripts"
$tools        = "C:\Tools"
$dev          = "C:\Dev"
$dev_net      = Join-Path $dev "Net"
$dev_projects = Join-Path $dev_net "Projects"
$dev_oss      = Join-Path $dev_net "OSS"

function Get-ModulesPath($modulename) {
  return Join-Path $modules $modulename
}

$userprofile = Get-Content env:\USERPROFILE
if (($HOME -eq "") -or ($HOME -eq "H:\") -or ($HOME -eq "h:\")) {
  Remove-Item -Force variable:\home
  $home = $userprofile
  (Get-PSProvider 'FileSystem').Home = $home
}

if (Test-Path "$root\locale_profile.ps1") { . "$root\locale_profile.ps1" }

# Prepend scripts directory to PATH environment variable to have powershell find the scripts
# We use 'dot sourcing' to bring Prepend-Path to the shell without having to execute the script every time
. $scripts\prepend-path.ps1 $scripts
#. $scripts\cd.ps1

# http://stackoverflow.com/questions/10438508/error6-while-trying-to-use-sublime-text-to-msbuild
Set-Alias subl (Join-Path $root "sublime_text.bat")

# Haskell (Stack exec shortcuts)
function ghc { stack exec -- ghc $args }
function ghci { stack exec -- ghci $args }
function ghce { stack exec -- $args }

# *nix style commands (using 'Git for Windows' as 'mini Cygwin')
#

# Remove curl/wget alias to Invoke-WebRequest, because it is blocking curl/wget *nix tool
if (Test-Path alias:curl) { Remove-Item alias:curl -force }
set-alias curlp Invoke-WebRequest
if (Test-Path alias:wget) { Remove-Item alias:wget -force }
set-alias wgetp Invoke-WebRequest

if (Test-Path 'C:\Tools\cygwin\bin') {
    # Add Git distributed *nix tools to path (prepended)
    prepend-path 'C:\Tools\cygwin\bin'
}
elseif (Test-Path 'C:\Program Files\Git\usr\bin') {
    # Add Git distributed *nix tools to path (prepended)
    prepend-path 'C:\Program Files\Git\usr\bin'
}
else {
    $red = [ConsoleColor]::Red
    Write-Host "No Unix tools found (both Cygwin and 'Git for Windows' cannot be found)." -f $red
}
function whereis($name) {
  &where.exe $name #windows tool
}
function lsa($glob) {
  # list all files and directories (ignoring . and ..)
  &'C:\Program Files\Git\usr\bin\ls.exe' -Al --file-type --group-directories-first --color=auto -- $glob # *nix tool
}
function lsf($glob) {
  # list files only
  &'C:\Program Files\Git\usr\bin\ls.exe' -al -- $glob | grep ^[-l] # with symbolic links
}
# directories only
function lsd($glob) {
  # list all directories (ignoring . and ..)
  &'C:\Program Files\Git\usr\bin\ls.exe' -AlF -- $glob | grep ^d
}
set-alias sudo Invoke-Elevated

# These are part of Git-Unix tools
#
#function touch {New-Item "$args" -ItemType File}
# function which($name)
# {
#     Get-Command $name | Select-Object -ExpandProperty Definition
# }

# Remove ri alias because it is blocking Ruby ri documentation tool
if (Test-Path alias:ri) { Remove-Item alias:ri -force }

# Add git-tfs to path
if (Test-Path 'C:\Tools\Git-Tfs') {
  prepend-path 'C:\Tools\Git-Tfs'
}

$windowsIdentity = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$windowsPrincipal = new-object 'System.Security.Principal.WindowsPrincipal' $windowsIdentity

function Add-Signature {
  param([string] $file=$(throw "Please specify a filename."))
  $cert = @(Get-ChildItem cert:\CurrentUser\My -codesigning)[0]
  Set-AuthenticodeSignature $file $cert
}

function show-path { $env:path.split(";") }

function Get-PSVersion {
  if (test-path variable:psversiontable) {
    $PSVersionTable.PSVersion
  }
  else {
    [version]"1.0.0.0"
  }
}

# chokolatey installer has done this
# $chokolateyDir = Join-Path $env:ALLUSERSPROFILE 'chocolatey'
# echo $chokolateyDir
# if (Test-Path $chokolateyDir) {
#     prepend-path (Join-Path $chokolateyDir 'bin')
# }

# .NET Framework 4.5.x is installed
function Test-Net45 {
  if (Test-Path 'HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full') {
    if (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full' -Name Release -ErrorAction SilentlyContinue)
    {
      return $True
    }
    return $False
  }
}

# Replace cd alias with a cd function that keeps a stack of the pwd-history
if (Test-Path Alias:cd) { Remove-Item Alias:cd }
$GLOBAL:dirStack = new-object System.Collections.Generic.Stack[System.String]
function cd {
  if ($args[0] -eq '-') {
    if ($GLOBAL:dirStack.Count -gt 0) {
      $newPwd=$GLOBAL:dirStack.Pop()
    }
    else {
      $newPwd = $false
    }
  }
  elseif ($args[0] -eq '--history') {
    $newPwd = $false
    $index = 1
    foreach($dir in $dirStack)
    {
      Write-Host "    ${index}: ${dir}"
      $index = $index + 1
    }
  }
  else {
    if ($args[0]) {
      $newPwd=$args[0];
    }
    else {
      # cd (with no arguments) is equal to cd ~
      $newPwd=$home;
    }
    $pwd = (Get-Location).Path
    if ($newPwd -ne $pwd) {
      $GLOBAL:dirStack.Push($pwd)
    }
    else {
      $newPwd = $false
    }
  }
  if ($newPwd) {
    Set-Location -LiteralPath $newPwd;
  }
}

${function:u} = { cd .. }
#${function:..} = { cd .. }
${function:...} = { cd ..\.. }
${function:....} = { cd ..\..\.. }
${function:.....} = { cd ..\..\..\.. }
${function:......} = { cd ..\..\..\..\.. }
${function:.......} = { cd ..\..\..\..\..\.. }

# functions unsuitable for aliases
function cd~ { cd ~ }
function cd.. { cd .. }
function cd... { cd ..\.. }
function cd.... { cd ..\..\.. }
function cd..... { cd ..\..\..\.. }
function cd...... { cd ..\..\..\..\.. }
function cd....... { cd ..\..\..\..\..\.. }

# Sublime Text aliases
$sublimeDataDir = Join-Path "${env:AppData}" "Sublime Text 3"
if (Test-Path $sublimeDataDir) {
  function subld {cd "$sublimeDataDir"}
  function sublp {cd (Join-Path "$sublimeDataDir" "Packages")}
  function sublu {cd (Join-Path "$sublimeDataDir" "Packages\User")}
}
#Remove-Variable sublimeDataDir

# Home alias
function home { cd $home }

# Powershell aliases
function psd { cd $(Split-Path -parent $profile) }

# Projects alias
function p          { cd $(Join-Path $home "Projects") }
function pnet       { cd $dev_projects }
function oss        { cd $dev_oss }

# alias function to directories
function docs       { cd (Join-Path $userprofile "Documents") }
function tools      { cd $tools }

function automapper { cd (Join-Path $dev_oss "AutoMapper") }
function spark      { cd (Join-Path $dev_oss "Spark") }

function borbas     { cd (Join-Path $dev_projects "Borbas") }
function lofus      { cd (Join-Path $dev_projects "lofus") }
function ffv        { cd (Join-Path $dev_projects "FFV-RTL") }
function ffve       { cd (Join-Path $dev_projects "FFV-RTL-Example") }
function ffvp       { cd (Join-Path $dev_projects "FFV-RTL-F3F5") }

#PsGet: Search and install PowerShell modules easy
Import-Module PsGet

###############################################################################
# psreadline and posh-git
# Adapted from https://dl.dropboxusercontent.com/u/41823/psh/Profile.txt
# http://www.reddit.com/r/sysadmin/comments/1rit4l/what_do_you_get_when_you_cross_bash_with_cmdexe/cdo3djk
###############################################################################

# Set up a simple prompt, adding the git prompt parts inside git repos
function global:prompt {

}

if ($PSVersionTable.PSVersion.Major -ge 4) {

    if ($host.Name -eq 'ConsoleHost') {

        # Windows 10 has builtin PSReadLine
        if (Get-Module | ? { $_.name -like 'psreadline' }) {
            Import-Module PSReadline
        }

        Import-Module posh-git

        Set-PSReadlineKeyHandler -Key Ctrl+Delete     -Function KillWord
        Set-PSReadlineKeyHandler -Key Ctrl+Backspace  -Function BackwardKillWord
        Set-PSReadlineKeyHandler -Key Shift+Backspace -Function BackwardKillWord
        Set-PSReadlineKeyHandler -Key UpArrow         -Function HistorySearchBackward
        Set-PSReadlineKeyHandler -Key DownArrow       -Function HistorySearchForward
        Set-PSReadlineKeyHandler -Key Tab             -Function Complete

        $Host.PrivateData.ErrorBackgroundColor   = $Host.UI.RawUI.BackgroundColor
        $Host.PrivateData.WarningBackgroundColor = $Host.UI.RawUI.BackgroundColor
        $Host.PrivateData.VerboseBackgroundColor = $Host.UI.RawUI.BackgroundColor

        function Prompt {
            #$c = [ConsoleColor]::Cyan
            #$c = [ConsoleColor]::Yellow
            #$c = [ConsoleColor]::Magenta
            $c = [ConsoleColor]::Blue

            Write-Host($pwd.ProviderPath) -n -f $c

            $realLASTEXITCODE = $LASTEXITCODE
            Write-VcsStatus # posh-git
            $global:LASTEXITCODE = $realLASTEXITCODE

            Write-Host ">" -n -f $c

            return " "
        }

    } # end of ConsoleHost

    # Updating PATH to prevent 'WARNING: Could not find ssh-agent'
    # Note: Commented out 'Start-SshAgent -Quiet' in posh-git/profile.example.ps1
    # prepend-path ((Get-Item "Env:ProgramFiles(x86)").Value + "\Git\bin")
    Set-Alias ssh-agent "${env:ProgramFiles}\git\usr\bin\ssh-agent.exe"
    Set-Alias ssh-add "${env:ProgramFiles}\git\usr\bin\ssh-add.exe"

    Start-SshAgent -Quiet

} else {
  write-host "You are running a version of Powershell less than v4 -- PsReadLine cannot be imported!" -foregroundcolor yellow
}

# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}

function Get-File-Exists-On-Path([string]$file)
{
    $results = ($env:Path).Split(";") | Get-ChildItem -filter $file -erroraction silentlycontinue
    $found = ($results -ne $null)
    return $found
}

# Setup hub aliasing: git will alias hub, this way git will be extended with hub verb-commands

if (-not (Get-File-Exists-On-Path "hub.exe")) {
    Write-Warning "hub.exe could not be found in your PATH. Please add it to your PATH."
}
# See http://chriskirby.net/blog/easily-alias-hub-as-git-on-windows
if ( (-not (Test-Path Alias:git)) -and (Get-File-Exists-On-Path "hub.exe")){
    Set-Alias git hub
}

##########################################
# Pop-Environment, Push-Enviroment hacks
##########################################
# Returns the current environment.
function Get-Environment {
  get-childitem Env:
}
# Restores the environment to a previous state.
function Restore-Environment {
  param(
    [parameter(Mandatory=$TRUE)]
      [System.Collections.DictionaryEntry[]] $oldEnv
  )
  # Remove any added variables.
  compare-object $oldEnv $(Get-Environment) -property Key -passthru |
  where-object { $_.SideIndicator -eq "=>" } |
  foreach-object { remove-item Env:$($_.Name) }
  # Revert any changed variables to original values.
  compare-object $oldEnv $(Get-Environment) -property Value -passthru |
  where-object { $_.SideIndicator -eq "<=" } |
  foreach-object { set-item Env:$($_.Name) $_.Value }
}
###################################################################################
# These utility functions have been extracted from the Pscx.Utility.psm1 module
# of the Powershell Community Extensions, which is here: http://pscx.codeplex.com/
###################################################################################
<#
.SYNOPSIS
    Invokes the specified batch file and retains any environment variable changes it makes.
.DESCRIPTION
    Invoke the specified batch file (and parameters), but also propagate any
    environment variable changes back to the PowerShell environment that
    called it.
.PARAMETER Path
    Path to a .bat or .cmd file.
.PARAMETER Parameters
    Parameters to pass to the batch file.
.PARAMETER RedirectStdErrToNull
    Whether to redirect the stderr of the batch file to null. Necessary for some scripts such
    as the Windows SDK SetEnv.cmd script. Defaults to $false for compatibility with other calls
    to this function.
.EXAMPLE
    C:\PS> Invoke-BatchFile "$env:ProgramFiles\Microsoft Visual Studio 9.0\VC\vcvarsall.bat"
    Invokes the vcvarsall.bat file.  All environment variable changes it makes will be
    propagated to the current PowerShell session.
.NOTES
    Author: Lee Holmes (slight modifications by JN)
#>
Function Invoke-BatchFile
{
    param([string]$Path, [string]$Parameters, [bool]$RedirectStdErrToNull = $false)
    $tempFile = [IO.Path]::GetTempFileName()
    ## Store the output of cmd.exe.  We also ask cmd.exe to output
    ## the environment table after the batch file completes
    if ($RedirectStdErrToNull -eq $true) {
        (cmd.exe /c " `"$Path`" $Parameters && set > `"$tempFile`" ") 2> $null
    } else {
        cmd.exe /c " `"$Path`" $Parameters && set > `"$tempFile`" "
    }
    ## Go through the environment variables in the temp file.
    ## For each of them, set the variable in our local environment.
    Get-Content $tempFile | Foreach-Object {
        if ($_ -match "^(.*?)=(.*)$")
        {
            Set-Content "env:\$($matches[1])" $matches[2]
        }
    }
    Remove-Item $tempFile
}
# http://blogs.msdn.com/b/ploeh/archive/2008/04/09/visualstudio2008powershell.aspx
function Get-Batchfile ($file) {
    $cmd = "`"$file`" & set"
    cmd /c $cmd | Foreach-Object {
        $p, $v = $_.split('=')
        Set-Item -path env:$p -value $v
    }
}
# Invokes a Cmd.exe shell script and updates the environment.
function Invoke-CmdScript {
  param(
    [String] $scriptName
  )
  $cmdLine = """$scriptName"" $args & set"
  & $Env:SystemRoot\system32\cmd.exe /c $cmdLine |
  select-string '^([^=]*)=(.*)$' | foreach-object {
    $varName = $_.Matches[0].Groups[1].Value
    $varValue = $_.Matches[0].Groups[2].Value
    set-item Env:$varName $varValue
  }
}
<#
.SYNOPSIS
    Imports environment variables for the specified version of Visual Studio.
.DESCRIPTION
    Imports environment variables for the specified version of Visual Studio.
    This function requires the PowerShell Community Extensions. To find out
    the most recent set of Visual Studio environment variables imported use
    the cmdlet Get-EnvironmentVars.  If you want to revert back to a previous
    Visul Studio environment variable configuration use the cmdlet
    Pop-EnvironmentVars.
.PARAMETER VisualStudioVersion
    The version of Visual Studio to import environment variables for. Valid
    values are 2008, 2010 and 2012.
.PARAMETER Architecture
    Selects the desired architecture to configure the environment for.
  Defaults to x86 if running in 32-bit PowerShell, otherwise defaults to
  amd64.
.PARAMETER Configuration
    Selects the desired configuration in case of the Windows SDK. Defaults to
    Release if not specified.
.EXAMPLE
    C:\PS> Import-VisualStudioVars 2010
    Sets up the environment variables to use the VS 2010 compilers. Defaults
  to x86 if running in 32-bit PowerShell, otherwise defaults to amd64.
.EXAMPLE
    C:\PS> Import-VisualStudioVars 2012 arm
    Sets up the environment variables for the VS 2012 arm compiler.
#>
Function Import-VisualStudioVars
{
    param
    (
        [Parameter(Mandatory = $true, Position = 0)][ValidateSet('2010', '2012', '2013', 'WindowsSDK7.1')][string]$VisualStudioVersion,
        [Parameter(Position = 1)][string]$Architecture = 'amd64',
        [Parameter(Position = 2)][string]$Configuration = 'release'
    )
    End
    {
        switch ($VisualStudioVersion)
        {
            '2010' {
                #Push-Environment
                Invoke-BatchFile (Join-Path $env:VS100COMNTOOLS "..\..\VC\vcvarsall.bat") -Parameters $Architecture -RedirectStdErrToNull $false
            }
            '2012' {
                #Push-Environment
                Invoke-BatchFile (Join-Path $env:VS110COMNTOOLS "..\..\VC\vcvarsall.bat") -Parameters $Architecture -RedirectStdErrToNull $false
            }
            '2013' {
                #Push-Environment
                Invoke-BatchFile (Join-Path $env:VS120COMNTOOLS "..\..\VC\vcvarsall.bat") -Parameters $Architecture -RedirectStdErrToNull $false
            }
            'WindowsSDK7.1' {
                if ($Architecture -eq "amd64") {
                    $architectureParameter = "/x64"
                } elseif ($Architecture -eq "x86") {
                    $architectureParameter = "/x86"
                } else {
                    Write-Host "Unknown Configuration: $configuration"
                    return
                }
                if ($Configuration -eq "release") {
                    $configurationParameter = "/release"
                } elseif ($configuration -eq "debug") {
                    $configurationParameter = "/debug"
                } else {
                    Write-Host "Unknown Configuration: $configuration"
                    return
                }
                #Push-Environment
                Invoke-BatchFile (Join-Path $env:ProgramFiles "Microsoft SDKs\Windows\v7.1\Bin\setenv.cmd") -Parameters "$configurationParameter $architectureParameter" -RedirectStdErrToNull $true
            }
            default {
                Write-Error "Import-VisualStudioVars doesn't recognize VisualStudioVersion: $VisualStudioVersion"
            }
        }
    }
}
Function Get-GuessedVSVersion {
    #Platform SDK (since it seems to set VS100COMNTOOLS even without Visual Studio 2010 installed)
    if (Test-Path (Join-Path $env:ProgramFiles "Microsoft SDKs\Windows\v7.1\Bin\setenv.cmd")) {
        return 'WindowsSDK7.1'
    }
    #Visual Studio's, newest versions first
    #Visual Studio 2013
    if ((Test-Path env:\VS120COMNTOOLS) -and (Test-Path (Join-Path $env:VS120COMNTOOLS "..\..\VC\vcvarsall.bat"))) {
        return '2013'
    }
    #Visual Studio 2012
    if ((Test-Path env:\VS110COMNTOOLS) -and (Test-Path (Join-Path $env:VS110COMNTOOLS "..\..\VC\vcvarsall.bat"))) {
        return '2012'
    }
    #Visual Studio 2010
    if ((Test-Path env:\VS100COMNTOOLS) -and (Test-Path (Join-Path $env:VS100COMNTOOLS "..\..\VC\vcvarsall.bat"))) {
        return '2010'
    }
    throw "Can't find any of VS2010-2013 or WindowsSDK7.1."
}

# if (Get-GuessedVSVersion -eq "WindowsSDK7.1") {
#   Import-VisualStudioVars "WindowsSDK7.1"
# }

Set-Location $userprofile
