Execute mklink on the commandline to setup junction points (symlinks to posh-git and posh-hg)

mklink /J C:\Users\maxfire\Documents\WindowsPowerShell\Modules\posh-git C:\Dev\PowerShell\OSS\posh-git
mklink /J C:\Users\maxfire\Documents\WindowsPowerShell\Modules\posh-hg C:\Dev\PowerShell\OSS\posh-hg