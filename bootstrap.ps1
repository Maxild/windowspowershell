# Enable scripts (need run as admin)
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser -Confirm

# Install PsGet
(new-object Net.WebClient).DownloadString("http://psget.net/GetPsGet.ps1") | iex

# PsGet commands are different than the ones in PowershellGet (WMF5) which are built-in on Windows 10
# Both modules provide an install-module command. Therefore disable PowershellGet temporary while
# installing PsReadLine as follows:
if (Get-Module | ? { $_.name -like 'powershellget' }) {
	Remove-Module PowershellGet
}
Import-Module PsGet

Install-Module PSReadline

# Windows 10 (WMF5) has built-in PSReadLine. You can upgrade it by running this in CMD
#  powershell -noprofile -command "Install-Module -Force PSReadline"

# Install posh-git and posh-hg using PsGet (verify that git.exe and hg.exe can be run from powershell)
Install-Module posh-git
Install-Module posh-hg
