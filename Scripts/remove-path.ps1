$local:command_usage = 
"usage: remove-path path-to-be-removed
"

if ($args.length -lt 1) { return ($command_usage) }

$local:oldPath = get-content Env:\Path
$local:oldPathItems = $local:oldPath.split(";")
$local:newPathItems = @()
ForEach ($pathItem in $local:oldPathItems) { if ($pathItem -ne $args[0]) { $local:newPathItems = $local:newPathItems + $pathItem } }
$local:newPath = [string]::Join(";", $local:newPathItems) 
set-content Env:\Path $local:newPath